let http = require('http')

let port = 4000


let server = http.createServer(function(request, response){
	if(request.url == '/profile' && request.method == 'GET'){
		response.writeHead(200, {'Content-Type': 'application/json'})
		response.end('Welcome to Booking System')
	}

	if(request.url == '/courses' && request.method == 'GET'){
		response.writeHead(200, {'Content-Type': 'application/json'})
		response.end("Here’s our courses available")
	}

	if(request.url == '/addcourse' && request.method == 'GET'){
		response.writeHead(200, {'Content-Type': 'application/json'})
		response.end('Add a course to our resources')
	}

	/*if(request.url == '/addcourse' && request.method == 'POST'){
		response.writeHead(200, {'Content-Type': 'application/json'})
		response.end()
	}*/

	else {
		response.writeHead(404, {'Content-Type': 'text/plain'})
		response.end('Im sorry the page you are looking for cannot be found.')
	}

})

server.listen(port)
console.log(`Server is running at localhost:${port}`)


